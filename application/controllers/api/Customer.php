<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Customer extends REST_Controller {

    public function __construct($config = 'rest') {
        parent::__construct($config);
        $this->methods['index_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['index_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['index_put']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['index_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->database();
    }

    public function index_get() 
    {
        $id_customer = $this->get('customer_id') ?: $this->get('id');
        if ($id_customer == '') {
            $this->db->select("customer.*, user.username, user.password, user.email");
            $this->db->join('user','user.id = customer.user_id');
            $customer = $this->db->get('customer')->result();
        } else {
            $this->db->select("customer.*, user.username, user.password, user.email");
            $this->db->join('user','user.id = customer.user_id');
            $this->db->where('customer.id', $id_customer);
            $customer = $this->db->get('customer')->result();
        }

        if($customer){
            $this->response([
                'status'    => TRUE,
                'data'      => $customer
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response(array('status' => 'FALSE', REST_Controller::HTTP_NOT_FOUND));
        }
    }

    public function index_post()
    {
        $id_customer = $this->post('customer_id') ?: $this->post('id');
        $data = array();
        if ($this->post('name') != '') {
            $data['name']   = $this->post('name');
        }
        if ($this->post('phone') != '') {
            $data['phone']   = $this->post('phone');
        }
        if ($this->post('address') != '') {
            $data['address']   = $this->post('address');
        }
        if ($this->post('gender') != '') {
            $data['gender']   = $this->post('gender');
        }

        $this->db->where('id', $id_customer);
        $update = $this->db->update('customer', $data);

        if ($update) {
            $this->response($data,  REST_Controller::HTTP_OK);
        } else {
            $this->response(array('status' => 'FALSE', REST_Controller::HTTP_NOT_FOUND));
        }
    }
}
