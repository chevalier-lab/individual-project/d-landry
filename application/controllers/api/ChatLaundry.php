<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class ChatLaundry extends REST_Controller {

    public function __construct($config = 'rest') {
        parent::__construct($config);
        $this->methods['index_get']['limit'] = 1; // 500 requests per hour per user/key
        $this->methods['index_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['index_put']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['index_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->database();
    }

    public function index_get() 
    {
        $user_1_id = $this->get('user_1_id');
        $user_2_id = $this->get('user_2_id');
        if ($user_1_id == '' && $user_2_id == '') {
            $this->db->select('ch.*, cpengirim.name nama_pengirim, cpenerima.name nama_penerima');
            $this->db->from('chat ch');
            $this->db->join('user upengirim','ch.pengirim = upengirim.id','left');
            $this->db->join('user upenerima','ch.penerima = upenerima.id','left');
            $this->db->join('customer cpengirim', 'cpengirim.user_id = upengirim.id', 'left');
            $this->db->join('customer cpenerima', 'cpenerima.user_id = upenerima.id', 'left');
            $this->db->order_by('ch.waktu', 'DESC');
            $laundry = $this->db->get()->result();

        } else {
            // $this->db->where('id', $id_laundry);
            // $laundry = $this->db->get('laundry')->result();
            $this->db->select('ch.*, cpengirim.name nama_pengirim, cpenerima.name nama_penerima');
            $this->db->from('chat ch');
            $this->db->join('user upengirim','ch.pengirim = upengirim.id','left');
            $this->db->join('user upenerima','ch.penerima = upenerima.id','left');
            $this->db->join('customer cpengirim', 'cpengirim.user_id = upengirim.id', 'left');
            $this->db->join('customer cpenerima', 'cpenerima.user_id = upenerima.id', 'left');
            $this->db->where("(upengirim.id=$user_1_id AND upenerima.id=$user_2_id) OR (upengirim.id=$user_2_id AND upenerima.id=$user_1_id)");
            $this->db->order_by('ch.waktu', 'DESC');
            $laundry = $this->db->get()->result();

            if (count($laundry) > 0) {
                $name_laundry = "";
                $this->db->where("(user_id=$user_1_id) OR (user_id=$user_2_id)");
                $temp = $this->db->get("laundry")->row();
                $name_laundry = $temp->name;
                
                $index = 0;
                foreach ($laundry as $item) {
                    if ($item->nama_pengirim == null) {
                        $laundry[$index]->nama_pengirim = $name_laundry;
                    }
                    
                    if ($item->nama_penerima == null) {
                        $laundry[$index]->nama_penerima = $name_laundry;
                    }

                    $index += 1;
                }
            }
        }

        if($laundry){
            $this->response([
                'status'    => TRUE,
                'data'      => $laundry
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response(array('status' => 'FALSE', REST_Controller::HTTP_NOT_FOUND));
        }
    }

    public function index_post()
    {
        $data = array(
            'pesan'     => $this->post('pesan'),
            'pengirim'  => $this->post('pengirim'),
            'penerima'  => $this->post('penerima')
        );

        $insert = $this->db->insert('chat',$data);

        if ($insert) {
            $this->response($data, REST_Controller::HTTP_CREATED);
        }
    }
}