<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class TagihanKuota extends REST_Controller {

    public function __construct($config = 'rest') {
        parent::__construct($config);
        $this->methods['index_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['index_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['index_put']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['index_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->database();
    }

    public function index_get() 
    {
        $id_customer = $this->get('id');
        if ($id_customer == '') {
            $this->db->select('ck.*, tg.id tagihan_id, lp.name paket, lp.laundry_id, l.name laundry, pm.kuota_satuan, ck.status, pm.price, tg.bank, tg.no_rek');
            $this->db->join('laundry_paket lp','lp.id = ck.paket_id');
            $this->db->join('laundry l','l.id = lp.laundry_id');
            $this->db->join('laundry_paket_member pm', 'pm.laundry_paket_id = lp.id');
            $this->db->join('tagihan tg', 'tg.customer_kuota = ck.id');
            $customer = $this->db->get('customer_kuota ck')->result();
        } else {
            $this->db->select('ck.*, tg.id tagihan_id, lp.name paket, lp.laundry_id, l.name laundry, pm.kuota_satuan, ck.status, pm.price, tg.bank, tg.no_rek');
            $this->db->join('laundry_paket lp','lp.id = ck.paket_id');
            $this->db->join('laundry l','l.id = lp.laundry_id');
            $this->db->join('laundry_paket_member pm', 'pm.laundry_paket_id = lp.id');
            $this->db->join('tagihan tg', 'tg.customer_kuota = ck.id');
            $this->db->where('customer_id', $id_customer);
            $customer = $this->db->get('customer_kuota ck')->result();
        }

        if($customer){
            $this->response([
                'status'    => TRUE,
                'data'      => $customer
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response(array('status' => 'FALSE', REST_Controller::HTTP_NOT_FOUND));
        }
    }

    public function index_post()
    {
        $id_tagihan = $this->post("id_tagihan");
        $bukti_pembayaran = $this->post("bukti_pembayaran");
        
        $this->db->where("id", $id_tagihan);
        $this->db->update("tagihan", array(
            "bukti_pembayaran"  => $bukti_pembayaran
        ));

        $this->db->where("id", $id_tagihan);
        $customer = $this->db->get("tagihan")->row();

        $this->db->where("id", $customer->customer_kuota);
        $this->db->update("customer_kuota", array(
            "status"    => 1
        ));

        if($customer){
            $this->response([
                'status'    => TRUE,
                'data'      => $customer
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response(array('status' => 'FALSE', REST_Controller::HTTP_NOT_FOUND));
        }
    }

}