<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Layanan extends REST_Controller {

    public function __construct($config = 'rest') {
        parent::__construct($config);
        $this->methods['index_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['index_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['index_put']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['index_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->database();
    }

    public function index_get() 
    {
        $laundry_id = $this->get('laundry_id');
        $id = $this->get('id');
        if ($laundry_id == '' && $id == '') {
            $this->db->select('p.*, l.id as laundry_id, l.name as laundry_name, l.phone, l.address, l.owner_name');
            $this->db->from('laundry_layanan p');
            $this->db->join('laundry l','l.id = p.laundry_id','left');
            $pemesanan = $this->db->get()->result();
        }
        else if ($laundry_id == '' && $id != '') {
            $this->db->select('p.*, l.id as laundry_id, l.name as laundry_name, l.phone, l.address, l.owner_name');
            $this->db->from('laundry_layanan p');
            $this->db->join('laundry l','l.id = p.laundry_id','left');
            $this->db->where('p.id', $id);
            $pemesanan = $this->db->get()->result();
        } else {
            $this->db->select('p.*, l.id as laundry_id, l.name as laundry_name, l.phone, l.address, l.owner_name');
            $this->db->from('laundry_layanan p');
            $this->db->join('laundry l','l.id = p.laundry_id','left');
            $this->db->where('p.laundry_id', $laundry_id);
            $pemesanan = $this->db->get()->result();
        }

        if($pemesanan){
            $this->response([
                'status'    => TRUE,
                'data'      => $pemesanan
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response(array('status' => 'FALSE', REST_Controller::HTTP_NOT_FOUND));
        }
    }
}
