<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class KuotaMember extends REST_Controller {

    public function __construct($config = 'rest') {
        parent::__construct($config);
        $this->methods['index_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['index_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['index_put']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['index_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->database();
    }

    public function index_get() 
    {
        $id_customer = $this->get('id');
        if ($id_customer == '') {
            $this->db->select('ck.*, lp.name paket, lp.laundry_id, l.name laundry, pm.kuota_satuan, ck.status, pm.price');
            $this->db->join('laundry_paket lp','lp.id = ck.paket_id');
            $this->db->join('laundry l','l.id = lp.laundry_id');
            $this->db->join('laundry_paket_member pm', 'pm.laundry_paket_id = lp.id');
            $customer = $this->db->get('customer_kuota ck')->result();
        } else {
            $this->db->select('ck.*, lp.name paket, lp.laundry_id, l.name laundry, pm.kuota_satuan, ck.status, pm.price');
            $this->db->join('laundry_paket lp','lp.id = ck.paket_id');
            $this->db->join('laundry l','l.id = lp.laundry_id');
            $this->db->join('laundry_paket_member pm', 'pm.laundry_paket_id = lp.id');
            $this->db->where('customer_id', $id_customer);
            $customer = $this->db->get('customer_kuota ck')->result();
        }

        if($customer){
            $this->response([
                'status'    => TRUE,
                'data'      => $customer
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response(array('status' => 'FALSE', REST_Controller::HTTP_NOT_FOUND));
        }
    }

    public function index_post()
    {
        $id_customer = $this->post('customer_id');
        $paket_id = $this->post('paket_id');
        if ($id_customer == '' || $paket_id == '') {
            $customer = null;
        } else {
            // $this->db->select('ck.*, lpm.kuota defaultKuota');
            // $this->db->join('laundry_paket_member lpm','lpm.laundry_paket_id = ck.paket_id');
            // $this->db->where('customer_id', $id_customer);
            // $this->db->where('paket_id', $paket_id);
            // $customer = $this->db->get('customer_kuota ck')->result();
            $data = array(
                'customer_id'   => $id_customer,
                'paket_id'      => $paket_id
            );
            $kuotaCustomerID = -1;
            // if (count($customer) > 0) {
            //     $data['kuota'] = ($customer[0]->kuota + $customer[0]->defaultKuota);
            //     $this->db->where('customer_id', $id_customer);
            //     $this->db->where('paket_id', $paket_id);
            //     $this->db->update('customer_kuota', $data);
            //     $customer = $data;

            //     $this->db->where('customer_id', $id_customer);
            //     $this->db->where('paket_id', $paket_id);
            //     $kuotaCustomerID = $this->db->get('customer_kuota')->row()->id;
            // }
            // else {
                $this->db->where('laundry_paket_id', $paket_id);
                $customer = $this->db->get('laundry_paket_member')->row();
                $data['kuota'] = $customer->kuota;
                $this->db->insert('customer_kuota', $data);
                $kuotaCustomerID = $this->db->insert_id();
                $customer = $data;
            // }
            $this->db->insert('tagihan', array(
                'customer_kuota'    => $kuotaCustomerID,
                'bank'              => $this->post('bank'),
                'no_rek'            => $this->post('no_rek')
            ));
        }

        if($customer){
            $this->response([
                'status'    => TRUE,
                'data'      => $customer
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response(array('status' => 'FALSE', REST_Controller::HTTP_NOT_FOUND));
        }
    }
}
